#!/usr/bin/env python3

import pathlib, re


def process_phone_number(m):
    return m.group(1) + m.group(2).translate(str.maketrans("", "", " -()"))


if __name__ == "__main__":
    contacts = pathlib.Path("collections/contacts/46aba013-1517-a9a6-6284-815ca2e523ed")

    for file in contacts.glob("*.vcf"):
        text = file.read_text()
        # name = re.search(r"^FN:(.*)$", text, re.MULTILINE).group(1)

        text = text.replace("cell", "CELL")
        text = text.replace("home", "HOME")
        text = text.replace("work", "WORK")
        text = text.replace("pref", "PREF")
        text = text.replace("TEL;TYPE=CELL", "TEL;TYPE=VOICE,CELL")
        text = text.replace("TEL;TYPE=HOME", "TEL;TYPE=VOICE,HOME")
        text = text.replace("TEL;TYPE=WORK", "TEL;TYPE=VOICE,WORK")

        text = re.sub(r";X-EVOLUTION-E164=[0-9,+]+:", ":", text)

        text = re.sub(r"(TEL;TYPE=VOICE,.+:)07", r"\1+447", text)
        text = re.sub(r"(TEL;TYPE=VOICE,.+:)01", r"\1+441", text)
        text = re.sub(
            r"TEL;TYPE=VOICE,HOME(,PREF)?:\+447", r"TEL;TYPE=VOICE,CELL\1:+447", text
        )
        text = re.sub(r"(TEL;.+:)(.+)", process_phone_number, text)

        new_text = ""
        lines = text.split("\n")
        numbers = []
        for i, line in enumerate(lines):
            match = re.fullmatch(r"TEL;TYPE=VOICE,.+:(\+\d+)", line)
            if match:
                number = match.group(1)
                if number in numbers:
                    continue
                numbers.append(number)

            new_text += line

            if i != len(lines) - 1:
                new_text += "\n"

        file.write_text(new_text)
