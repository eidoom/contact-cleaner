#!/usr/bin/env python3

import subprocess, string, argparse, os, pathlib, shutil

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-r", "--url")
    parser.add_argument("-u", "--username")
    parser.add_argument("-p", "--password")
    parser.add_argument("--reset-local", action="store_true")
    args = parser.parse_args()

    base = pathlib.Path("vdirsyncer")

    if args.reset_local:
        print("Deleting local collections")
        shutil.rmtree(base / "status")
        shutil.rmtree(pathlib.Path("collections"))

    config_file = base / "config"

    if not config_file.is_file():
        config_data = {name: args[name] for name in ("url", "username", "password")}
        config_template_file = base / "config.template"

        config_template = config_template_file.read_text()

        config_text = string.Template(config_template).substitute(config_data)

        config_file.write_text(config_text)

    env = {**os.environ, "VDIRSYNCER_CONFIG": str(config_file)}

    subprocess.run(["vdirsyncer", "discover", "my_contacts"], env=env)

    subprocess.run(["vdirsyncer", "sync"], env=env)
