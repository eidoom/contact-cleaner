#!/usr/bin/env python3

import pathlib, subprocess, os

import jinja2

if __name__ == "__main__":
    khard = pathlib.Path("khard")
    env = jinja2.Environment(loader=jinja2.FileSystemLoader(khard))
    template = env.get_template("khard.conf.jinja2")

    contacts = [
        {"name": contact.stem, "path": contact.as_posix()}
        for contact in pathlib.Path("collections/contacts").iterdir()
    ]

    conf = template.render({"contacts": contacts})

    pathlib.Path("khard/khard.conf").write_text(conf)

    env = {**os.environ, "XDG_CONFIG_HOME": "."}

    subprocess.run(["khard"], env=env)
