# [contact-cleaner](https://gitlab.com/eidoom/contact-cleaner/)

```shell
source init-venv.bash

./sync.py -h  # set options to pull remote contacts to local
./clean.py  # consistent phone number syntax, remove duplicate numbers on a single contact
./print.py  # pretty print contacts
./sync.py  # push changes to remote
```
